﻿class TitleScene extends Phaser.Scene {
	constructor() {
		super("titleScene");
	}

	preload() {
		this.load.spritesheet('towerSheet', 'images/towers.png', { frameWidth: 128, frameHeight: 128 });
		this.load.image('towers', 'images/towers.png');
		this.load.tilemapTiledJSON('level1', 'images/level_0001.json');
	}

	create() {
		this.socket = io();
		this.add.text(20, 20, "Click to continue");
	}

	update() {
		var pointer = this.input.activePointer;
		if (pointer.isDown) {
			this.scene.start('levelOne');
		}
	}
}

class LevelOneScene extends Phaser.Scene {
	showPath = false;

	constructor() {
		super("levelOne");
	}

	create() {
		var map = this.make.tilemap({ key: 'level1' });
		var tiles = map.addTilesetImage('towers', 'towers');
		var ground = map.createStaticLayer('Ground', tiles, 0, 0);
		var flora = map.createStaticLayer('Flora', tiles, 0, 0);
		this.cameras.main.setBounds(0, 0, 2048, 1152);

		this.input.on('pointerdown', (pointer) => {
			var cam = this.cameras.main;
			cam.pan(this.input.mousePointer.worldX, this.input.mousePointer.worldY, 250);
		}, this);


		//this.add.image(0, 0, 'towers', 0);
		/*
				var graphics = this.add.graphics();
				var path;
		
				// the path for our enemies
				// parameters are the start x and y of our path
				path = this.add.path(96, -32);
				path.lineTo(96, 164);
				path.lineTo(480, 164);
				path.lineTo(480, 544);
		
				graphics.lineStyle(3, 0xffffff, 1);
				// visualize the path
				path.draw(graphics);*/
	}
}

var config = {
	type: Phaser.AUTO,
	parent: 'phaser-example',
	width: window.innerWidth,
	height: window.innerHeight,
	physics: {
		default: 'arcade',
		arcade: {
			debug: false,
			gravity: { y: 0 }
		}
	},
	scene: [TitleScene, LevelOneScene]
};

var game = new Phaser.Game(config);
